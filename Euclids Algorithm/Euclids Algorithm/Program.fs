﻿open System

Console.WriteLine ("Enter the 2 numbers you want to use in euclids algorithm")

let x = Console.ReadLine() //Take in user input X
let y = Console.ReadLine() //Take in user input y
let X : int = int32 x //ensure user input of type int
let Y : int = int32 y //ensure user input of type int

let rec euclid a b =
    match b with
    |0 -> a
    |_ -> printfn "%A %A" a b; euclid b (a % b) //calculates euclids algorithm while showing steps

[<EntryPoint>]
let main argv = 
    if X < 0 || Y < 0 //ensures inouts are positive integers
    then printfn "Enter Valid Value" //error message
     else printfn("The GCD is: %d") (euclid X Y) //prints out answer
    0 // return an integer exit code
