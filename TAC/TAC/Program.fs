﻿module tac = 
    open System
    open System.IO

    let doesExist path=
        if not (File.Exists(path)) then
            false
        else
            true

    let rec reverse file =
        match file with
            |x::xs -> printfn "%s" x ; reverse xs //reverse list and prints out string 
            | _ -> printfn ""
                       
    let readFile path =
        let Open = File.OpenRead(path) //opens file for reading
        let file = List.rev(Array.toList (File.ReadAllLines(path)))// converts string array into a list and reverse list
        reverse file

    [<EntryPoint>]
    let main argv=
        Array.iter readFile argv
        printf "\n"
        0 // return an integer exit code























//let getFilename (path:string) =  Array.last (path.Split [|'\\'|]) //Retrieves the name of the file being used splits the file path into an array using \ as a seperator and then gets the last value of the array which is the file name 


//let readlines =
//    use stream = new StreamReader @"C:\Users\akals\Documents\University\Year 2\Semester A\Declarative Programming\Assignments\TAC\TAC\Test.txt"

// 
// 
//

//    let rec reverse f =
//        match f with
//            |x::xs -> reverse xs ; printfn "%s" x //reverse list and prints out string 
//            | _ -> printfn ""