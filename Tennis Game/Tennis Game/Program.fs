﻿open System 

type points = Love =0 | Fifteen=1 | Thirty=2 | Forty=3 | Duce=4 | Advantage=5 | Win =6 //Type points that contains the points that can be scored in a game of tennis 

type name = string

type player = name * points //type of player

let playerName (x: player) = fst x //gets name of player from type

let playerScore (x: player) = snd x //gets score of player from type

let pointGame (player1:player) (player2:player) = //match statement deals with the flow of points within the game 
    match playerScore player1 with
        | points.Love -> ((playerName player1, points.Fifteen), player2)
        | points.Fifteen -> ((playerName player1, points.Thirty), player2)
        | points.Thirty when playerScore player2 = points.Forty -> ((playerName player1, points.Duce),(playerName player2, points.Duce)) //when forty forty go to deuce
        | points.Thirty -> ((playerName player1, points.Forty), player2)
        | points.Forty -> ((playerName player1, points.Win), player2)
        | points.Advantage -> ((playerName player1, points.Win), player2)
        | points.Duce when playerScore player2 = points.Advantage -> ((playerName player1, points.Duce), (playerName player2, points.Duce)) //if advantage looses point back down to deuce
        | points.Duce -> ((playerName player1, points.Advantage), player2)

let random = (new System.Random()).Next(2) //random number generator
let updateScore (player1:player) (player2:player) = if (new System.Random()).Next(2) = 1 then pointGame player1 player2 else pointGame player2 player1 //randomly updates score depending on recieved random number if 2 then pointGame fipped

let rec mainGame (player1:player) (player2:player) = 
    match (player1,player2) with
    |((_,points.Win),(_,_)) -> printfn "Player %A has won" (playerName player1) ; exit 0 //check if p1 has won 
    |((_,_),(_,points.Win)) -> printfn "Player %A has won" (playerName player2) ; exit 0 //check if p2 won
    |((_,_),(_,_))-> ignore()
   
    printfn "%s : %A || %s : %A" (playerName player1) (playerScore player1) (playerName player2) (playerScore player2) //displays player scores 
    let first_player, second_player = updateScore player1 player2
    Console.ReadLine() //user press enter for next point
    mainGame first_player second_player

[<EntryPoint>]
let main argv = 
    let player1:player = ("Player1",points.Love) //player 1 details
    let player2:player = ("Player2",points.Love) //player 2 details
    mainGame player1 player2
    0 // return an integer exit code