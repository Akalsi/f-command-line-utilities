﻿module wc = 
    
    open System
    open System.IO

    let doesExist path= //check to see if the file path exists
        if not (File.Exists(path)) then
            false
        else
            true

    let getFilename (path:string) =  Array.last (path.Split [|'\\'|]) //Retrieves the name of the file being used splits the file path into an array using \ as a seperator and then gets the last value of the array which is the file name 

    let rec readBytes (s:FileStream) (path) (x:int) (y:int) (n:int):unit = // x= words y= bytes n= new_lines
        let c = s.ReadByte() in
            match c with
            | -1 -> printfn "File Name: %A" (getFilename path)
                    printfn "Words %i" x //When end of the file is reached print out words, bytes and new lines 
                    printfn "Bytes %i" y
                    printfn "New Lines %i" n
            | _ -> 
                let b = Char.ConvertFromUtf32 c 
                match b with 
                |" " -> readBytes s path (x+1) (y+1) n //if character is a space add 1 to the number of words and to the byte count 
                |"\n" -> readBytes s path (x+1) (y+1) (n+1) //If a new line is matched add one to the number of words, byte count and new line count
                |_ -> readBytes s path x (y+1) n //if anything else is recognised add 1 to the bytes
               
    let printFile (path:string):unit =
        if doesExist path then 
            use stream = File.OpenRead(path)
            readBytes stream path 0 0 0 
        else 
            printfn "File Does Not Exist"
 
    [<EntryPoint>]
        let main argv=
             Array.iter printFile argv 
             0 // return an integer exit code
